#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

std::string writeMadLib(std::string words[])
{
	std::string madLib;

	madLib = "One day it was very " + words[0] + " outside ,so me and my friends decided to go swimming down at the " + words[1] + ".";
	madLib = madLib + "We packed some " + words[2] + " and " + words[3] + ", got in the " + words[4] + ", and drove. When we got there, ";
	madLib = madLib + "we applied plenty of " + words[5] + " before going out in the sun. It was fun playing " + words[6] + " in the water.";
	madLib = madLib + "Afterward, we stopped at " + words[7] + " to get some ice cream. All and all it was a " + words[8] + " day";
	return(madLib);
}

int main()
{
	std::string path = "C:\\Users\\Eric\\Documents\\test.txt";
	std::ofstream ofs(path);

	const int numWords = 9;
	char choice;
	std::string words[numWords] = { "n adjective"," place"," noun"," noun"," vehical"," noun"," game"," place","n adjective" };
	std::string userWords[numWords];
	std::string madLib;

	for (int c = 0; c < numWords; c++)
	{
		std::cout << "Enter a" << words[c] << " ";
		std::cin >> userWords[c];
	}

	madLib = writeMadLib(userWords);

	std::cout << "\n" << madLib << "\n";

	std::cout << "Would you like to save this madlib to a text file? (y/n)";
	std::cin >> choice;

	if (choice == 'y')
	{
		ofs << madLib;
	}

	ofs.close();

	_getch();

	return(0);
}